<h1 align="center">Memorandum - Pandoc Template</h1>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]() 
[![Version](https://img.shields.io/badge/version-0.0.1-informational)](./CHANGELOG.md) 
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](./LICENSE)

</div>

---

<p align="center"> Template and build system to create a Memorandum using Pandoc, LaTex and LuaTex.
    <br> 
</p>

<p align="center">
  <a href="#about">🧐 About</a> •
  <a href="#getting_started">🏁 Getting Started</a> •
  <a href="#usage">🤹 Usage</a> •
  <a href="#changelog">📜 Changelog</a> •
  <a href="#todo">✅ Todo</a> •
  <a href="#built_using">🔨 Built Using</a> •
  <a href="#contributing">⚙️ Contributing</a> •
  <a href="#credits">✍️ Credits</a> •
  <a href="#support">🤲 Support</a> •
  <a href="#license">⚖️ License</a>
</p>

![Screenshot](./screenshot.png)

## 🧐 About <a name="about"></a>

This is a template and build system to create a Memorandum using Pandoc, LaTex and LuaTex.

This is one of many templates in my [Pandoc Templates](#) collection.

## 🏁 Getting Started <a name="getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and local generation of documents.

### 🦺 Prerequisites

Before proceeding make sure you have the following tools installed and updated on your system:

- [Pandoc](https://pandoc.org/installing.html).
- [LaTex](https://www.latex-project.org/get/).
- [LuaTex](http://luatex.org/download.html), PdfLatex or XeLatex.
- [Make](https://www.gnu.org/software/make/)

### 📦 Installing

#### 1. Clone the repository

Open a terminal and clone this repository to your computer using git:

```sh
git clone https://gitlab.com/thegrojas/pandoc-memo.git
```
Then `cd` into the cloned repo:

```sh
cd pandoc-memo
```

#### 2. Change the `Makefile` to suit your needs

I'm using LuaTex as my PDF engine for LaTex but you can use PdfLatex or XeLatex. Just change the value of `PDF_ENGINE` inside `Makefile`.

Also please note that I use a **non-standard** path for my `.cls` and `.sty` files. Please change the value of `TEXMFHOME` so that it installs those files on the correct folder on your system.

#### 3. Install using `make`

Just run the following command inside this repository to install all the `.cols` and `.latex` files:

```sh
make install
```

## 🤹 Usage <a name="usage"></a>

**Tip**: I set a help system on the `Makefile`. Just type `make` or `make help` and you will get a list of useful targets.

Now, let's use the `sample.md` file as an example. First, let's take a look at the front matter:

```yaml
---
documentclass: memo
fontsize: 12pt
title: Memorandum
memo:
  to: Noah
  from: God
  subject: Regarding the Flood
  date: A long time ago
  # logo: # No logo since God doesn't have a logo (as far as I know)
...
```

Here is an explanation of these fields:

- `documentclass` Defines the LaTex document class. This should always be set to *memo*.
- `fontsize` Can be change up to 12pt. Since this template uses the default Pandoc Latex template you should be able to use any of the other options available on that template. More about that [here](https://pandoc.org/MANUAL.html)
- `title` Can be changed to whatever you want but I suggest keeping it as it is.
- `memo` is an object that contains the key/value pairs to for the final front matter.
  - `to` Recipient of this memo.
  - `from` Author of this memo.
  - `subject` Subject of the memo. Try to keep it short and sweet.
  - `date` Date when this memo will be published or distributed. Use any format you want.
  - `logo` Path to a .png that will be used as a logo in the upper right corner. This is optional.

After this it's just a matter of typing the body of the memo and save the file.

Once the file is saved I will generate the .pdf using make:

```
make sample.pdf
```

## 📜 Changelog <a name="changelog"></a>

All notable changes to this project will be documented in `CHANGELOG.md`. Click [here](./CHANGELOG.md) to take a look.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## ✅ Todo <a name="todo"></a>

Pending tasks will be tracked in `TODO.md`. Click [here](./TODO.md) to take a look

## 🔨 Built Using <a name="built_using"></a>

- [Make](https://www.gnu.org/software/make/) - Build System
- [LaTex](https://www.latex-project.org/) - Typesetting System
- [LuaLatex](http://luatex.org/) - PDF Generation Engine
- [Pandoc](https://pandoc.org/index.html) - Document Conversion System

## ⚙️ Contributing <a name="contributing"></a>

Developers, Testers and Translators are all welcomed. For more information on how to contribute to this project please refer to `CONTRIBUTING.md`. Click [here](./CONTRIBUTING.md) to take a look.

## ✍️ Credits <a name="credits"></a>

- [Rob Oaks](http://www.oak-tree.us/) is the author of the TexMemo class used on this [template](https://www.latextemplates.com/template/memo).
- [@kylelobo](https://github.com/kylelobo) creator of [The Documentation Compendium](https://github.com/kylelobo/The-Documentation-Compendium) whose templates I used as inspiration for my README's.


See also the list of [contributors](#) who participated in this project.

## 🤲 Support <a name="support"></a>

For now, just by sharing and/or staring this repository you are supporting my work.

## ⚖️ License <a name="license"></a>

This code is released under the [GLPv3](./LICENSE) license. For more information refer to `LICENSE.md`
