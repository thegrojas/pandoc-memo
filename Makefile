.DEFAULT_GOAL := help

TARGET=memo

TEXMFHOME=~/.texmf/tex/latex/local
PANDOC_TEMPLATE_DIR=~/.pandoc/templates

PANDOC_FLAGS =\
	--template memo.latex \
	-f markdown+tex_math_single_backslash \
	-t latex \

LATEX_FLAGS = \

PDF_ENGINE = lualatex
PANDOCVERSIONGTEQ2 := $(shell expr `pandoc --version | grep ^pandoc | sed 's/^.* //g' | cut -f1 -d.` \>= 2)
ifeq "$(PANDOCVERSIONGTEQ2)" "1"
    LATEX_FLAGS += --pdf-engine=$(PDF_ENGINE)
else
    LATEX_FLAGS += --latex-engine=$(PDF_ENGINE)
endif

.PHONY: help

help: ## Prints help for Makefile.
	@echo "These are the available targets for this Makefile: \n"
	@grep -E '^[a-zA-Z_-%]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

install: ## Installs .latex, .cls and .sty files to Pandoc's template directory and Texfm default directory.
	@mkdir -p $(PANDOC_TEMPLATE_DIR)
	@cp $(TARGET).latex $(PANDOC_TEMPLATE_DIR)/$(TARGET).latex
	@mkdir -p $(TEXMFHOME)
	@cp $(TARGET).cls $(TEXMFHOME)/$(TARGET).cls

build: $(TARGET).pdf ## Builds pdf to current directory using default settings.
	@echo $(TARGET).pdf generated succesfully

%.pdf: %.md ## Builds a .pdf file from a .md file. Where % is the name of the file.
	pandoc $(PANDOC_FLAGS) $(LATEX_FLAGS) -o $@ $<

%.tex: %.md ## Builds a .tex file from a .md file. Where % is the name of the file.
	pandoc --standalone $(PANDOC_FLAGS) -o $@ $<

view: $(TARGET).pdf ## Opens .pdf file with default pdf viewer.
	@if [ "Darwin" = "$(shell uname)" ]; then open $(TARGET).pdf ; else xdg-open $(TARGET).pdf ; fi


clean: ## Removes .aux .log .nav. out. .snm .toc and .vrb files.
	@rm -f *.aux *.log *.nav *.out *.snm *.toc *.vrb || true

veryclean: clean ## Removes all ouput files.
	@rm -f *.pdf

print: $(TARGET).pdf ## Sends all .pdf files to local printer.
	lpr $(TARGET).pdf

variable-%: ## Print Makefile variable %. Eg: make print-TARGET will print the contents of the variable TARGET.
	@echo $*=$($*)
