---
documentclass: memo
fontsize: 12pt
title: Memorandum
memo:
  to: Noah
  from: God
  subject: Regarding the Flood
  date: A long time ago
  logo: placeholder-logo.png
...

I’ve decided to destroy every living thing on earth, because it has become filled with violence due to them. Look! I’m about to annihilate them, along with the earth. 

So make yourself an ark out of cedar, constructing compartments in it, and cover it inside and out with tar. Make the ark like this: 300 cubits long, 50 cubits wide, and 30 cubits high. Make a roof for the ark, and finish the walls to within one cubit from the top. Place the entrance in the side of the ark, and build a lower, a middle, and an upper deck.

For my part, I’m about to flood the earth with water and destroy every living thing that breathes. Everything on earth will die. However, I will establish my own covenant with you, and you are to enter the ark —you, your sons, your wife, and your sons’ wives. 

You are to bring two of every living thing into the ark so they may remain alive with you. They are to be male and female. From birds according to their species, from domestic animals according to their species, and from everything that crawls on the ground according to their species —two of everything will come to you so they may remain alive. For your part, take some of the edible food and store it away—these stores will be food for you and the animals.
