---
documentclass: memo
title: Memorandum
memo:
  to: # Recipient of this memo.
  from: # Author of this memo.
  subject: # Subject of the memo. Try to keep it short and sweet.
  date: # Date when this memo will be published or distributed.
  logo: # Path to a .png that will be used as a logo in the upper right corner.
...

The Body of your memo goes here. Remember this is markdown, you still have access to *fancy* **styling**.

If the memo is too long I suggest you to add a **TL;DR** at the beginning of the body.
